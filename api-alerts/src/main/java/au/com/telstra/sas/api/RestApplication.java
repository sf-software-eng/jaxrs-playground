package au.com.telstra.sas.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAX-RS application config.
 */
@ApplicationPath("v2")
public class RestApplication extends Application
{
}
