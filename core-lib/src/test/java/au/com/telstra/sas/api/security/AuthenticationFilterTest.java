package au.com.telstra.sas.api.security;

import java.io.IOException;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * Unit tests for AuthenticationRequestFilter.
 * 
 * @author d236359
 */
public class AuthenticationFilterTest
{
	/**
	 * Test missing auth header (return 401).
	 */
	@Test
	public void missingAuthHeader()
	{
		// Mock request context to return no auth header.
		ContainerRequestContext requestContext = Mockito.mock(ContainerRequestContext.class);
		Mockito.when(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn(null);
		
		// Mock authorization provider.
		AuthorizationProvider authorizationProvider = Mockito.mock(AuthorizationProvider.class);
		Mockito.when(authorizationProvider.getChallenge()).thenReturn("Basic realm=test");

		Assertions.assertThrows(NotAuthorizedException.class, () ->
		{
			AuthenticationFilter authFilter = new AuthenticationFilter();
			authFilter.authorizationProvider = authorizationProvider;
			authFilter.filter(requestContext);
		});
	}
	
	/**
	 * Test invalid auth header (return 401).
	 */
	@Test
	public void invalidAuthHeader() throws Exception
	{
		final String CREDENTIALS = "Basic pretend-i-am-base64";
		
		// Mock request context to return dummy authorization header.
		ContainerRequestContext requestContext = Mockito.mock(ContainerRequestContext.class);
		Mockito.when(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn(CREDENTIALS);
		
		// Mock authorization provider to throw AuthorizationException.
		AuthorizationProvider authorizationProvider = Mockito.mock(AuthorizationProvider.class);
		Mockito.when(authorizationProvider.validateCredentials(CREDENTIALS)).thenThrow(AuthorizationException.class);
		Mockito.when(authorizationProvider.getChallenge()).thenReturn("Basic realm=test");

		Assertions.assertThrows(NotAuthorizedException.class, () ->
		{
			AuthenticationFilter authFilter = new AuthenticationFilter();
			authFilter.authorizationProvider = authorizationProvider;
			authFilter.filter(requestContext);
		});
	}
	
	/**
	 * Test unexpected auth error (return 500).
	 */
	@Test
	public void serverError() throws Exception
	{
		final String CREDENTIALS = "Basic pretend-i-am-base64";
		
		// Mock request context to return dummy authorization header.
		ContainerRequestContext requestContext = Mockito.mock(ContainerRequestContext.class);
		Mockito.when(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn(CREDENTIALS);
		
		// Mock authorization provider to throw arbitrary exception.
		AuthorizationProvider authorizationProvider = Mockito.mock(AuthorizationProvider.class);
		Mockito.when(authorizationProvider.validateCredentials(CREDENTIALS)).thenThrow(IOException.class);
		Mockito.when(authorizationProvider.getChallenge()).thenReturn("Basic realm=test");

		Assertions.assertThrows(InternalServerErrorException.class, () ->
		{
			AuthenticationFilter authFilter = new AuthenticationFilter();
			authFilter.authorizationProvider = authorizationProvider;
			authFilter.filter(requestContext);
		});
	}
	
	/**
	 * Test successful authentication (should result in request SecurityContext being updated).
	 */
	@Test
	public void validJwt() throws Exception
	{
		final String CREDENTIALS = "Basic pretend-i-am-base64";
		final User EXPECTED_USER = new User("me", Role.ADMIN, "1", "company");

		// Mock request context to return dummy authorization header.
		ContainerRequestContext requestContext = Mockito.mock(ContainerRequestContext.class);
		Mockito.when(requestContext.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn(CREDENTIALS);
		
		// Mock authorization provider to return test user.
		AuthorizationProvider authorizationProvider = Mockito.mock(AuthorizationProvider.class);
		Mockito.when(authorizationProvider.validateCredentials(CREDENTIALS)).thenReturn(EXPECTED_USER);
		Mockito.when(authorizationProvider.getChallenge()).thenReturn("Bearer realm=test");
		
		// Configure mocked ContainerRequestContext to validate User when setSecurityContext(...) is called.
		Mockito.doAnswer(invocation -> 
		{
			SecurityContext securityContext = invocation.getArgument(0);
			Assertions.assertEquals(EXPECTED_USER, (User)securityContext.getUserPrincipal());
			return null;
		}).when(requestContext).setSecurityContext(Mockito.any());

		// And finally - run the filter.
		AuthenticationFilter authFilter = new AuthenticationFilter();
		authFilter.authorizationProvider = authorizationProvider;
		authFilter.filter(requestContext);
	}
}
