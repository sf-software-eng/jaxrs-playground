package au.com.telstra.sas.api.security;

import java.util.stream.Stream;

/**
 * Enumeration of available user roles.
 */
public enum Role
{
	/**
	 * Tenant read-only access.
	 */
	VIEWER,

	/**
	 * Tenant read/write access.
	 */
	USER,

	/**
	 * Tenant administrator.
	 */
	ADMIN,

	/**
	 * Superuser read-only access (all tenants).
	 */
	PORTAL_VIEWER,

	/**
	 * Superuser read/write access (all tenants).
	 */
	PORTAL_USER,

	/**
	 * Superuser administrator access (all tenants).
	 */
	PORTAL_ADMIN;

	/**
	 * Init.
	 */
	Role()
	{
		// The API name is the enum name converted to lower-case, and with hyphens instead of underscores.
		this.apiName = name().toLowerCase().replace('_', '-');

		// If the API name starts with "portal-" then this is a super-user role.
		this.superUser = this.apiName.startsWith("portal-");
	}

	/**
	 * Is this a super-user role?
	 */
	public boolean isSuperUser()
	{
		return superUser;
	}

	/**
	 * Get API name for role.
	 */
	public String getApiName()
	{
		return apiName;
	}

	/**
	 * Get role for the given API name, or null if no match found.
	 */
	public static Role fromApiName(String apiName)
	{
		return Stream.of(Role.values())
			.filter(role -> role.apiName.equalsIgnoreCase(apiName))
			.findFirst()
			.orElse(null);
	}

	// Superuser role flag.
	private final boolean superUser;

	// The role name as it appears in API calls, JWTs, etc.
	private final String apiName;
}
