package au.com.telstra.sas.config;

import lombok.Getter;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;

/**
 * Container configuration data.
 */
@ApplicationScoped
@Getter
public class ContainerConfig
{
	// Host/FQDN for Elasticsearch queries.
	@Inject @ConfigProperty(name = "ELASTICSEARCH_HOST")
	private String elasticsearchHost;

	// Port for Elasticsearch queries.
	@Inject @ConfigProperty(name = "ELASTICSEARCH_PORT")
	private int elasticsearchPort;

	// User for Elasticsearch queries.
	@Inject @ConfigProperty(name = "ELASTICSEARCH_USER")
	private String elasticsearchUser;

	// Password for Elasticsearch queries.
	@Inject @ConfigProperty(name = "ELASTICSEARCH_PWD")
	private String elasticsearchPassword;

	// Keystore path.
	@Inject @ConfigProperty(name = "TRUST_STORE_PATH")
	private String trustStorePath;

	// Keystore password.
	@Inject @ConfigProperty(name = "TRUST_STORE_PWD")
	private String trustStorePwd;

	// HTTP client timeout (seconds, optional).
	@Inject @ConfigProperty(name = "HTTP_CLIENT_TIMEOUT_SECONDS")
	private Optional<Integer> httpClientTimeoutSeconds;
}