package au.com.telstra.sas.config;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;

import javax.enterprise.context.ApplicationScoped;

/**
 * Global key/value configuration data provider.
 * 
 * @author d236359
 */
@ApplicationScoped
@Deprecated // FIXME: To be replaced with Microprofile Config...
public class KeyValueConfig
{
	/**
	 * Default constructor for injected instances - loads key/value data from the environment.
	 * 
	 * NB: If the source of configuration data changes later (ServletContext init parameters, System properties,
	 *     properties file, etc) then this is where the update should happen.
	 */
	public KeyValueConfig()
	{
		this.data.putAll(System.getenv());
	}
	
	/**
	 * Explicit constructor for mocking configuration in unit tests. Input is provided as an array of key/value
	 * pairs, eg:
	 * 
	 * new KeyValueConfig(
	 * 	"KEY_1", "VALUE_1",
	 *  "KEY_2", "VALUE_2",
	 *  ...)
	 */
	public KeyValueConfig(String... keyValuePairs)
	{
		for (int i = 0; i < keyValuePairs.length; i += 2)
			this.data.put(keyValuePairs[i], keyValuePairs[i + 1]);
	}
	
	/**
	 * Get string value by key, or null if none.
	 */
	public String getString(String key)
	{
		return data.get(key);
	}
	
	/**
	 * Get string value by key and convert it into the parameterized type with the provided function, or null if no value.
	 * NB: This method may throw a runtime exception if the conversion function fails.
	 */
	public <T> T get(String key, Function<String, T> converter)
	{
		String string = getString(key);
		return (string == null) ? null : converter.apply(string);
	}
	
	/**
	 * Get string array value by key, or null if none.
	 * NB: Individual values in the source string are expected to be comma-separated, and any leading or trailing 
	 *     whitespace around individual values will be trimmed. 
	 */
	public String[] getStringArray(String key)
	{
		return get(key, string -> STRING_ARRAY_DELIMITER_PATTERN.split(string.trim()));
	}

	/**
	 * Get integer value by key, or null if none.
	 */
	public Integer getInteger(String key)
	{
		return get(key, Integer::valueOf);
	}

	// Key/value data.
	private final Map<String, String> data = new HashMap<>();
	
	// String array delimiter regex.
	private static final Pattern STRING_ARRAY_DELIMITER_PATTERN = Pattern.compile("\\s*,\\s*");
}
