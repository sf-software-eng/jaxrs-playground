package au.com.telstra.sas.feature.email;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.core.MediaType;
import java.util.Date;

@ApplicationScoped
public class MailClient
{
    public void sendMail(String to, String subject, String content) throws Exception
    {
        MimeMessage message = new MimeMessage(mailSession);
        message.setSubject(subject);
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSentDate(new Date());
        message.setContent(content, MediaType.TEXT_PLAIN);
        Transport.send(message);
    }

    @Resource(lookup = "mail/internal")
    private Session mailSession;
}
