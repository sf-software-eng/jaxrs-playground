package au.com.telstra.sas.api;

import lombok.extern.log4j.Log4j2;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.util.Properties;

/**
 * Application version information API.
 */
@Log4j2
@Path("version")
public class VersionResource
{
	/**
	 * Fetch version information.
	 */
	@GET @Produces(MediaType.APPLICATION_JSON)
	public JsonObject getVersionInfo() throws Exception
	{
		try (InputStream in = getClass().getResourceAsStream("/version.properties"))
		{
			// Load version.properties file.
			Properties properties = new Properties();
			properties.load(in);

			// Copy contents into JSON object, and return to caller.
			JsonObjectBuilder jsonVersionInfo = Json.createObjectBuilder();
			properties.forEach((key, value) -> jsonVersionInfo.add(key.toString(), value.toString()));
			return jsonVersionInfo.build();
		}
	}
}
