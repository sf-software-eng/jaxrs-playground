package au.com.telstra.sas.api;

import au.com.telstra.sas.feature.alerts.Alert;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Output from AlertResource (is automatically converted into JSON).
 */
@Getter
@ToString
public class AlertListResponse
{
	// The message returned to the user.
	private final List<Alert> alerts = new ArrayList<>();

}
