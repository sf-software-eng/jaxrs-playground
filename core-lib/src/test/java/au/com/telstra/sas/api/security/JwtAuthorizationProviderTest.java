package au.com.telstra.sas.api.security;

import au.com.telstra.sas.config.KeyValueConfig;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Date;

/**
 * Unit tests for JwtAuthorizationProvider.
 * 
 * @author d236359
 */
public class JwtAuthorizationProviderTest
{
	/**
	 * Initialize test material.
	 */
	@BeforeAll
	public static void init() throws Exception
	{
		// Create secret key for HS256 signing.
		hs256Key = new SecretKeySpec(
			Base64.getDecoder().decode(HS256_KEY_MATERIAL),
			SignatureAlgorithm.HS256.getJcaName());
		
		// Create secret key for HS384 signing.
		hs384Key = new SecretKeySpec(
			Base64.getDecoder().decode(HS384_KEY_MATERIAL),
			SignatureAlgorithm.HS384.getJcaName());
		
		// Create RSA private key for RS256 signing.
		KeySpec rsaPrivateKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(RS256_PRIVATE_KEY_MATERIAL));
		rs256PrivateKey = KeyFactory.getInstance("RSA").generatePrivate(rsaPrivateKeySpec);
		
		// Create EC private key for ES256 signing.
		KeySpec ecPrivateKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(ES256_PRIVATE_KEY_MATERIAL));
		es256PrivateKey = KeyFactory.getInstance("EC").generatePrivate(ecPrivateKeySpec);
		
		// Create invalid secret key for HS256 signing (mangle the source base64 data a little).
		hs256InvalidKey = new SecretKeySpec(
			Base64.getDecoder().decode(HS256_KEY_MATERIAL.replace('e', 'f')),
			SignatureAlgorithm.HS256.getJcaName());

		// Initialize new JwtAuthorizationProvider with our test keys for HS256, RS256 and ES256.
		KeyValueConfig config = new KeyValueConfig(
			"JWT_ALGORITHMS", "HS256,RS256,ES256",
			"JWT_HS256_KEY", HS256_KEY_MATERIAL,
			"JWT_RS256_KEY", RS256_PUBLIC_KEY_MATERIAL,
			"JWT_ES256_KEY", ES256_PUBLIC_KEY_MATERIAL);
		jwtAuthorizationProvider = new JwtAuthorizationProvider(config);
	}
	
	/**
	 * Helper: Create authorization credentials string for TARGET_USER and provided JWT parameters.
	 */
	private static String createCredentials(SignatureAlgorithm algorithm, Key key, boolean expired)
	{
		// Load user claims into JWT builder.
		JwtBuilder jwt = Jwts.builder()
			.claim(JwtAuthorizationProvider.USER_ID_CLAIM, EXPECTED_USER.getId())
			.claim(JwtAuthorizationProvider.USER_ROLE_CLAIM, EXPECTED_USER.getRole())
			.claim(JwtAuthorizationProvider.TENANT_ID_CLAIM, EXPECTED_USER.getTenantId())
			.claim(JwtAuthorizationProvider.TENANT_NAME_CLAIM, EXPECTED_USER.getTenantName());
		
		// If expired then set expiration time-stamp to the epoch.
		if (expired)
			jwt.setExpiration(new Date(0L));
		
		// Apply signature algorithm, if applicable.
		if ((algorithm != null) && (key != null))
			jwt.signWith(key, algorithm);
		
		// Generate HTTP authorization credentials string. 
		return "Bearer " + jwt.compact();
	}
	
	/**
	 * Test invalid credentials string.
	 */
	@Test
	public void invalidCredentials()
	{
		Assertions.assertThrows(AuthorizationException.class, () ->
		{
			jwtAuthorizationProvider.validateCredentials("Basic pretend-i-am-base64");
		});
	}

	/**
	 * Test invalid JWT (algorithm NONE).
	 */
	@Test
	public void invalidNoSignature()
	{
		Assertions.assertThrows(AuthorizationException.class, () ->
		{
			jwtAuthorizationProvider.validateCredentials(createCredentials(null, null, false));
		});
	}
	
	/**
	 * Test invalid JWT (algorithm not configured).
	 */
	@Test
	public void invalidAlgorithm()
	{
		Assertions.assertThrows(AuthorizationException.class,  () -> 
		{
			jwtAuthorizationProvider.validateCredentials(createCredentials(SignatureAlgorithm.HS384, hs384Key, false));
		});
	}
	
	/**
	 * Test invalid JWT (expired).
	 */
	@Test
	public void invalidExpired()
	{
		Assertions.assertThrows(AuthorizationException.class,  () ->
		{
			jwtAuthorizationProvider.validateCredentials(createCredentials(SignatureAlgorithm.HS256, hs256Key, true));
		});
	}
	
	/**
	 * Test invalid JWT (bad signature).
	 */
	@Test
	public void invalidSignature()
	{
		Assertions.assertThrows(AuthorizationException.class,  () ->
		{
			jwtAuthorizationProvider.validateCredentials(createCredentials(SignatureAlgorithm.HS256, hs256InvalidKey, false));
		});
	}

	/**
	 * Test valid HS256 signed JWT.
	 */
	@Test
	public void validHs256() throws Exception
	{
		User actualUser = jwtAuthorizationProvider.validateCredentials(
			createCredentials(SignatureAlgorithm.HS256, hs256Key, false));
		Assertions.assertEquals(EXPECTED_USER, actualUser);
	}
	
	/**
	 * Test valid RS256 signed JWT.
	 */
	@Test
	public void validRs256() throws Exception
	{
		User actualUser = jwtAuthorizationProvider.validateCredentials(
			createCredentials(SignatureAlgorithm.RS256, rs256PrivateKey, false));
		Assertions.assertEquals(EXPECTED_USER, actualUser);
	}
	
	/**
	 * Test valid ES256 signed JWT.
	 */
	@Test
	public void validEs256() throws Exception
	{
		User actualUser = jwtAuthorizationProvider.validateCredentials(
			createCredentials(SignatureAlgorithm.ES256, es256PrivateKey, false));
		Assertions.assertEquals(EXPECTED_USER, actualUser);
	}

	// Test key material.
	private static final String HS256_KEY_MATERIAL =
		"zeIf+A1j1s8o0fuD2EUF+mf0ZHDfRUv/8z0jS9SpnMw=";
	private static final String HS384_KEY_MATERIAL =
		"svrn24n1/4AzV/mJ8ha+nf2Nf52Kza9RkO0iBX0vUXYZUiFplekNLN/wmI/02rHm";
	private static final String RS256_PUBLIC_KEY_MATERIAL =
		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiSVys2F+XfaL98MPfwE+FUJb/XVZOkiGfiiM" +
			"ZQgdaJsawqRob8DBCpJg2bU8Elhtwrp7oDMbrEGk+gR2xyUkQq1RMjncBOvg4PnNsw8kCZ0fWJLoT30H" +
			"EquSy0vDA2ZJ3FDlmjP3D9yDhDK8Hvo+9v/X/WI1n4FqYhOvOqECNHAi+b7wJjbrMDaklJ2v2medkTsA" +
			"1873bHmxwNpJVBlQuMTDTuegfj/EiG/joWlPy2ji0oSFGL/hIPKvlIvcFz30TViqwDA1HI/4GebLu9PD" +
			"EOgpAFiJvH09IQWQO1rxi7EqkZ1exmAXubsLEe1A6XEqijtOU5cgMzsc+WvLBk3x/QIDAQAB";
	private static final String RS256_PRIVATE_KEY_MATERIAL =
		"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCJJXKzYX5d9ov3ww9/AT4VQlv9dVk6" +
			"SIZ+KIxlCB1omxrCpGhvwMEKkmDZtTwSWG3CunugMxusQaT6BHbHJSRCrVEyOdwE6+Dg+c2zDyQJnR9Y" +
			"kuhPfQcSq5LLS8MDZkncUOWaM/cP3IOEMrwe+j72/9f9YjWfgWpiE686oQI0cCL5vvAmNuswNqSUna/a" +
			"Z52ROwDXzvdsebHA2klUGVC4xMNO56B+P8SIb+OhaU/LaOLShIUYv+Eg8q+Ui9wXPfRNWKrAMDUcj/gZ" +
			"5su708MQ6CkAWIm8fT0hBZA7WvGLsSqRnV7GYBe5uwsR7UDpcSqKO05TlyAzOxz5a8sGTfH9AgMBAAEC" +
			"ggEAfo8+P+cSNCfWlnc9D5jX78pjFYx7INvYsQc+N+W7LdMkaMZLUgVwnw7hrUYf/1X3RCHO/9DqEpYp" +
			"ZkdETRYrVhSWv2DBdxEtXq9ncXvhZ7XFPXmkQ+Y+5q3KMyzg83wrYNKObzTRI6P9AbvH+lY4ULtcWImO" +
			"rnlA8Oz1YYc/f5845LRh3CpnoxjlD5FcFfEfqpod7LXknzjp9Okb4pHfEY/X0GK1LcvL9iVwxlGznGIj" +
			"W5wplo53l+RNTCwJojzVt4ykd2yImfSW1/P7tzLNeVQMrxFSNmWloAJ1z+v1qc+Kk4H7YpFFXrOyIAPr" +
			"wai3aV5Ohc2oDHSD8fPn0h/zyQKBgQDKnopsckSliyoXif/7mFmkrIW3jgSyb8TcqB7v4bMH3gK2OQeD" +
			"oyrEGXs4V4mr3mb8lQpgk+qWIquJDQwKnkYp/s/Uz7e4PQbTLtHqMzaSXKWD7F02dNelYoLvy3Y++PWo" +
			"3GAW9pcEdzJdLlH3OzGh5moOEB4THdw2T4vdfxHdZwKBgQCtRyTgJem9QL4wpI7W6W/fmTXUgZtC39ke" +
			"5P4F+6h0m89uC1d3dsypZVdNH55K3UMNz47csPUH6qEJST50Ng0zYBMGOn39kc4r834/tAQ1c/SZBr/h" +
			"gguOSgtvrATgzm3FcGjKmU3/mABBpTz6kZbSLJxDdGnUfzv4FZqRWf5y+wKBgQC0e55f1TVSpHC9AP7V" +
			"FLMdHVQK2eVTMH4R82uDT+8SXe7adFg+KLSqcuwwlsEaKARijzvsJTX7wCw96zJDrxJezAxCyzgiU7Hl" +
			"oTn4vN+qZf5FUeEB5fhE6Egr43eTxl3EqXCqY8LHhomFOQJoEAE47q6fkjnH23xRoG+8wDiglwKBgBQD" +
			"IHYmys0Jui2CA+E8SqhnWHk3ksfxdHEF/3SPcRAuopFSDAMHHHH/+0oz+aMp1MG2lAhXA6WranfZFrZz" +
			"O6kQUoIopT5w5X/b+lu/HbFSuBAoYGbaDx2a3aFDG7ke/jyMU/36ILEBTB5IyDckM2gIsO91dXeOTk3D" +
			"qPtainGbAoGAO8Ll+JtqyDOLSLm8ssHmZs4oaMGJxsTYzyOAzEv3MgfjQ5QXJBd3X7j7dcjm3gEyJ/H5" +
			"M6Di0d8Nx39qefA4JAcNC/aFq0wYvhoPqhZX1VaHCBzL1XjUKBqDpMXWCzeNgb1AXHj5KUoXrlD+eMDT" +
			"DGuFI79ivPqypZPkCcQi2UQ=";
	private static final String ES256_PUBLIC_KEY_MATERIAL =
		"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE9qOnCn8cXbnr/BWJBJnahpkCMC5e369ONbwm+igF7Nd/" +
			"N3k05inx49xXj/iAjotiJopABYW7l9L87uG4M7b4lA==";
	private static final String ES256_PRIVATE_KEY_MATERIAL =
		"MEECAQAwEwYHKoZIzj0CAQYIKoZIzj0DAQcEJzAlAgEBBCC5Nc3cKZifPydLSYxWwl3ciA3xzjRENdaB" +
			"wK16djYqQg==";

	// The user that successful tests are expected to produce.
	private static final User EXPECTED_USER = new User("me", Role.ADMIN, "1", "company");

	// Configured JwtAuthorizationProvider for testing, plus a selection of keys.
	private static JwtAuthorizationProvider jwtAuthorizationProvider;
	private static Key hs256Key;
	private static Key hs384Key;
	private static Key rs256PrivateKey;
	private static Key es256PrivateKey;
	private static Key hs256InvalidKey;
}
