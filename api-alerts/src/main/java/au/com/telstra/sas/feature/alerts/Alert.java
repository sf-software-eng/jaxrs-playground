package au.com.telstra.sas.feature.alerts;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Encapsulates alert information.
 *
 */
@Getter
@Setter
@ToString
public class Alert {

    private String id;
    private int priority;
    private String impact;
    private String severity;
    private String classification;
    private String alertType;
    private String logType;
    private String eventCount;
    private String device;
    private String site;
    private String countryName;
    private String utcCreated;
    private String utcEventStart;
}


