package au.com.telstra.sas.api.security;

import java.security.Principal;

import lombok.Data;

/**
 * Encapsulates user information provided by the AuthenticationRequestFilter.
 * 
 * FIXME: Ideally I'd like to reduce this down to just the user ID. Everything else should be determined by querying RT DB.
 * 
 * @author d236359
 */
@Data
public class User implements Principal
{
	// User ID (email address or Telstra c/d-number).
	private final String id;
	
	// User role.
	private final Role role;
	
	// Tenant ID (CIDN).
	private final String tenantId;
	
	// Tenant name in RT.
	private final String tenantName;
	
	/**
	 * Required to implement Principal.
	 */
	@Override public String getName()
	{
		return id;
	}
}
