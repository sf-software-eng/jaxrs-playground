package au.com.telstra.sas.feature.alerts;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter @AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SeverityMap {

    private String severity;
    private int priority;
    private String priorityNumber;
    private String impact;
    private int severityNumber;

    private static final Map<String, SeverityMap> map = new HashMap<>();

    public static SeverityMap getBySeverity(String severity)
    {
        return map.get(severity);
    }

    static
    {
        Arrays.asList(
                new SeverityMap("critical", 1, "P1", "Extensive", 80),
                new SeverityMap("high", 2, "P2", "Significant", 60),
                new SeverityMap("medium", 3, "P3", "Moderate", 40),
                new SeverityMap("low", 4, "P4", "Minor", 20)
        ).forEach(s -> map.put(s.getSeverity(), s));
    }

}

