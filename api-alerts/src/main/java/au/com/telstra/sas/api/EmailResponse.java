package au.com.telstra.sas.api;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Output from EmailResource (is automatically converted into JSON).
 */
@Setter
@ToString
public class EmailResponse {

    String message = "";

}
