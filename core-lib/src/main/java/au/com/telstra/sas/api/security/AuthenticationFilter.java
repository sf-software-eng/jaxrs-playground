package au.com.telstra.sas.api.security;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

/**
 * JAX-RS request authentication filter.
 * 
 * @author d236359
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
@Secured
public class AuthenticationFilter implements ContainerRequestFilter
{
	/**
	 * Verify authentication information contained in incoming API request.
	 */
	@Override public void filter(ContainerRequestContext requestContext) throws IOException
	{
		// Fetch auth header from request, or return 401 if it's missing.
		String credentials = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (credentials == null)
			throw new NotAuthorizedException(authorizationProvider.getChallenge());
		
		try
		{
			// Validate credentials and update the request security context.
			User user = authorizationProvider.validateCredentials(credentials);
			SecurityContext securityContext = requestContext.getSecurityContext();
			boolean secure = (securityContext != null) && securityContext.isSecure();
			requestContext.setSecurityContext(new SimpleSecurityContext(secure, user));
		}
		catch (AuthorizationException ex)
		{
			// Return 401 response.
			throw new NotAuthorizedException(ex, authorizationProvider.getChallenge());
		}
		catch (Exception ex)
		{
			// Return 500 response.
			throw new InternalServerErrorException(ex);
		}
	}

	// HTTP Authorization header validation/parsing.
	@Inject AuthorizationProvider authorizationProvider;
}
