package au.com.telstra.sas.api;

import au.com.telstra.sas.api.security.Secured;
import au.com.telstra.sas.api.security.User;
import au.com.telstra.sas.feature.alerts.Alert;
import au.com.telstra.sas.feature.alerts.AlertDatabase;
import au.com.telstra.sas.feature.email.EmailService;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;


/**
 * API end-point.
 */
@Log4j2
@Path("alerts")
public class AlertResource {

    @Inject AlertDatabase db;

    @Inject EmailService emailService;

    @Path("customer")
    @GET @Produces(MediaType.APPLICATION_JSON)
    @Secured
    public AlertListResponse getAlertsByCustomer(
            @Context SecurityContext securityContext,
            @QueryParam("start") long start,
            @QueryParam("end") long end) throws Exception
    {

        User user = (User)securityContext.getUserPrincipal();
        List<Alert> resultData = db.getAllByDateRange(user.getTenantId(),start,end);

        AlertListResponse response = new AlertListResponse();
        response.getAlerts().addAll(resultData);

        return response;
    }

    @Path("{id}")
    @GET @Produces(MediaType.APPLICATION_JSON)
    @Secured
    public Alert getAlertById(
            @Context SecurityContext securityContext,
            @PathParam("id") String alertID) throws Exception
    {
        User user = (User)securityContext.getUserPrincipal();

        Alert resultData = db.getByID(user.getTenantId(),alertID);

        if (resultData == null)
            throw new NotFoundException();

        return resultData;
    }

    @Path("notify")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendEmail(
            List<Alert> alerts) throws Exception
    {
        log.debug(alerts.toString());

        return Response.noContent().build();
    }

}
