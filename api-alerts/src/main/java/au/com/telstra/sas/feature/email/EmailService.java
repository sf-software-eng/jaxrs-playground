package au.com.telstra.sas.feature.email;

import au.com.telstra.sas.feature.alerts.AlertDatabase;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class EmailService {

    @Inject
    MailClient mailClient;

    @Inject
    AlertDatabase db;

    public void notify(String alert_id) throws Exception {

        String to = "jose.ponce@team.telstra.com";
        String subject = "Spam Bot: Imperva Detection of allowed Spam Bot attacks";
        String text = "Spam Bot: Imperva Detection of allowed Spam Bot attacks";

        mailClient.sendMail(to,subject,text);

        updateAlert(alert_id);

    }

    public void updateAlert(String id) throws Exception{

        String jsonString = "{" +
                "\"notification.hint\":\"ignore\"," +
                "\"notification.sent\":\""+ System.currentTimeMillis() + "\"" +
                "}";

        db.updateAlertByID(id,jsonString);
    }

}
