package au.com.telstra.sas.api.security;

/**
 * Exception raised by AuthorizationProvider implementations if the provided credentials are invalid/unauthorized
 * (the throwing of this exception will trigger a 401 response back to the caller).
 * 
 * @author d236359
 */
public class AuthorizationException extends Exception
{
	public AuthorizationException(String message, Exception cause)
	{
		super(message, cause);
	}
}
