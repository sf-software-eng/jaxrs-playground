package au.com.telstra.sas.api;

import java.net.URI;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import lombok.extern.log4j.Log4j2;

/**
 * Catch-all JAX-RS exception mapper.
 * 
 * According to the spec anything that isn't WebApplicationException or handled 
 * explicitly by an ExceptionMapper is passed on to the parent Servlet container 
 * for handling. This can lead to undesirable behaviour (like stack traces being
 * returned to the client), so instead we make sure to handle all kinds of
 * exceptions explicitly here.
* 
 * @author d236359
 */
@Provider
@Log4j2
public class RestExceptionMapper implements ExceptionMapper<Exception>
{
	// Inject request URI information (for the log).
	@Context UriInfo uri;
	
	/**
	 * Convert exception to JAX-RS response.
	 */
	@Override public Response toResponse(Exception exception)
	{
		// Log all errors here.
		URI path = (uri == null) ? null : uri.getAbsolutePath();
		log.error("Error processing API request to " + path + " !", exception);
		
		if (exception instanceof WebApplicationException)
		{
			// Make sure server errors don't return any content. Otherwise default handling for REST exceptions.
			Response response = ((WebApplicationException)exception).getResponse(); 
			switch (response.getStatusInfo().getFamily())
			{
				case SERVER_ERROR: 
				case OTHER:
					return Response.status(response.getStatus()).build();
				default:
					return response;
			}
		}
		else
		{
			// Everything else is a 500 response.
			return Response.serverError().build();
		}
	}
}
