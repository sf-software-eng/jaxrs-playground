package au.com.telstra.sas.api.security;

import lombok.extern.log4j.Log4j2;

import javax.annotation.Priority;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * JAX-RS role based authorization filter.
 * FIXME: Finish refactoring this next year...
 * 
 * @author d236359
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
@Secured
@Log4j2
public class AuthorizationFilter implements ContainerRequestFilter
{
	/**
	 * Verify that the authenticated user attached to the incoming request is actually allowed to call that API.
	 */
	@Override public void filter(ContainerRequestContext requestContext)
	{
		log.debug("action: {}, indicators: {}", "security", requestContext.getHeaderString(HttpHeaders.AUTHORIZATION));

		// Extract the roles declared by the resource.
		List<Role> classRoles = extractRoles(resourceInfo.getResourceClass());
		List<Role> methodRoles = extractRoles(resourceInfo.getResourceMethod());

		try
		{
			// Check if the request is for tenant specific data
			if (!methodRoles.isEmpty() || !classRoles.isEmpty())
			{
				// Check if the user is allowed to execute the method
				// The method annotations override the class annotations
				if (methodRoles.isEmpty())
					checkPermissions(requestContext, classRoles);
				else
					checkPermissions(requestContext, methodRoles);
			}
		}
		catch (AuthorizationException ex)
		{
			// Abort the filter chain with a 401 status code response
			// The WWW-Authenticate header is sent along with the response
			// FIXME: This should probably be 403 Forbidden...
			log.error("Authorization check failed!", ex);
			requestContext.abortWith(Response
				.status(Response.Status.UNAUTHORIZED)
				.header(HttpHeaders.WWW_AUTHENTICATE, "Bearer realm=\"OpenMSS\"")
				.build());
		}
		catch(IOException | SQLException ex)
		{
			log.error("Authorization check failed!", ex);
			requestContext.abortWith(Response.serverError().build());
		}
	}

	private List<Role> extractRoles(AnnotatedElement annotatedElement)
	{
		if (annotatedElement == null)
			return Collections.emptyList();

		Secured secured = annotatedElement.getAnnotation(Secured.class);
		if (secured == null)
			return Collections.emptyList();

		return Arrays.asList(secured.value());
	}

	private void validateAuthorization(List<Role> allowedRoles, String user, Role role) {

		// Check that the user is authorised by comparing the role encoded in the group with the allowed roles
		// Throw an exception if user does not have the appropriate role
		if (!allowedRoles.contains(role == null ? "" : role))
		{
			log.error("Unauthorized - User: {}, Role: {}", user, role);
			throw new ForbiddenException();
		}
	}

	private void checkPermissions(ContainerRequestContext requestContext, List<Role> allowedRoles) throws IOException, SQLException, AuthorizationException {

		// Validate authorization
		User user = (User)requestContext.getSecurityContext().getUserPrincipal();
		validateAuthorization(allowedRoles, user.getId(), user.getRole());

		// Check super user authorization
		if (user.getRole().isSuperUser())
		{
			// Validate that the user has the claimed authorization
//			rtAccessQueries.validateSuperUserAuthorization(user.getId(), user.getRole().getApiName());

			// Only Telstra users should have super user access.
			if (!user.getId().endsWith("@team.telstra.com"))
				throw new ForbiddenException();
		}
		else
		{
			// Validate that the user has the claimed authorization
//			String group = buildGroupName(user.getTenantName(), user.getRole().getApiName());
//			String superGroup = buildSuperGroupName(user.getRole().getApiName());
//			rtAccessQueries.validateAuthorization(user.getId(), group, superGroup);
		}
	}

	// Information about the JAX-RS resource that the user is trying to access.
	@Context ResourceInfo resourceInfo;
}
