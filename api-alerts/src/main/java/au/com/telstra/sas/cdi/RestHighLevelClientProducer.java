package au.com.telstra.sas.cdi;

import au.com.telstra.sas.config.ContainerConfig;
import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.ssl.SSLContextBuilder;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.net.ssl.SSLContext;
import java.io.File;

/**
 * CDI producer for Elastic RestHighLevelClient instance, with configuration taken from ContainerConfig.
 */
@ApplicationScoped
@Log4j2
public class RestHighLevelClientProducer
{
	/**
	 * Fetch shared ES client instance.
	 */
	@Produces
	public RestHighLevelClient getRestHighLevelClient()
	{
		return restHighLevelClient;
	}

	/**
	 * Initialize producer.
	 */
	@PostConstruct
	void init()
	{
		log.debug("init: started...");

		// Initialize new ES client builder for configured host/port combination.
		HttpHost httpHost = new HttpHost(
			config.getElasticsearchHost(),
			config.getElasticsearchPort(),
			"https");
		RestClientBuilder restClientBuilder = RestClient.builder(httpHost);

		// Apply HTTP I/O timeout, if applicable.
		config.getHttpClientTimeoutSeconds().ifPresent(timeoutSeconds ->
		{
			int timeoutMs = timeoutSeconds * 1000;
			restClientBuilder.setRequestConfigCallback(requestConfigBuilder ->
			{
				return requestConfigBuilder
					.setConnectTimeout(timeoutMs)
					.setConnectionRequestTimeout(timeoutMs)
					.setSocketTimeout(timeoutMs);
			});
		});

		// Add node failure listener.
		restClientBuilder.setFailureListener(new RestClient.FailureListener()
		{
			@Override public void onFailure(Node node)
			{
				log.error("Elasticsearch node failed: {}", node);
			}
		});

		// Initialize robot credentials.
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(
			config.getElasticsearchUser(),
			config.getElasticsearchPassword()));

		// Add SSL/TLS context and robot credentials to client.
		SSLContext sslContext = newSslContext();
		restClientBuilder.setHttpClientConfigCallback(httpClientBuilder ->
		{
			return httpClientBuilder
				.setSSLContext(sslContext)
				.setDefaultCredentialsProvider(credentialsProvider);
		});

		// And *finally* - create the ES high level client!
		this.restHighLevelClient = new RestHighLevelClient(restClientBuilder);

		log.debug("init: completed");
	}

	/**
	 * Create new SSL/TLS context for the ES client.
	 */
	private SSLContext newSslContext()
	{
		try
		{
			String trustStorePath = config.getTrustStorePath();

			if(trustStorePath != "") {
				return (new SSLContextBuilder())
						.loadTrustMaterial(new File(config.getTrustStorePath()), config.getTrustStorePwd().toCharArray())
						.build();
			}else {
				return (new SSLContextBuilder())
						.loadTrustMaterial(new TrustSelfSignedStrategy())
						.build();
			}
		}
		catch (Exception ex)
		{
			log.error("Error initializing new SSL/TLS context!", ex);
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Producer shutdown.
	 */
	@PreDestroy
	void shutdown()
	{
		log.debug("shutdown: started...");
		try
		{
			restHighLevelClient.close();
			log.debug("shutdown: completed");
		}
		catch (Exception ex)
		{
			log.debug("shutdown: failed!", ex);
		}
	}

	// Container configuration, and single shared client instance.
	@Inject private ContainerConfig config;
	private RestHighLevelClient restHighLevelClient;
}
