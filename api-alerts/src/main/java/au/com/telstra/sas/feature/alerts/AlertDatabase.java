package au.com.telstra.sas.feature.alerts;

import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;

@Log4j2 @ApplicationScoped
public class AlertDatabase {

    @Inject RestHighLevelClient esClient;

    /**
     * Provide query functionality to get a list of tenant=specific alerts within a range of creation timestamps.
     *
     * @param tenantID - customer ID
     * @param start - beginning creation timestamp
     * @param end - ending creation timestamp
     * */
    public List<Alert> getAllByDateRange(String tenantID, long start, long end) throws Exception {
        List<Alert> listResults = new ArrayList<>();

        SearchRequest searchRequest = new SearchRequest("alert-*");
        QueryBuilder query = QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery("tenant.cidn", tenantID))
                .filter(QueryBuilders.rangeQuery("event.timestamp_detected").gte(start).lt(end));
        SearchSourceBuilder searchSourceBuilder = (new SearchSourceBuilder()).query(query);
        searchSourceBuilder.size(10000);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits searchHits = searchResponse.getHits();
        if (searchHits != null) {
            for(SearchHit hit :searchHits.getHits()) {

                    Alert newAlert = createAlertDataObject(hit);
                    listResults.add(newAlert);
            }
        }

        return listResults;
    }

    /**
     * Provide query functionality to get details of a tenant-specific alert
     *
     * @param tenantID - tenant ID
     * @param id - alert ID
     * */
    public Alert getByID(String tenantID, String id) throws Exception{

        SearchRequest searchRequest = new SearchRequest("alert-*");
        QueryBuilder query = QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery("tenant.cidn", tenantID))
                .must(QueryBuilders.idsQuery().addIds(id));
        SearchSourceBuilder searchSourceBuilder = (new SearchSourceBuilder()).query(query);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits searchHits = searchResponse.getHits();

        if(searchHits != null && searchHits.getHits().length > 0) {

                SearchHit hit = searchHits.getAt(0);
                Alert newAlert = createAlertDataObject(hit);

                return newAlert;

        }else {
            return null;
        }
    }

    /**
     * Provide update functionality to a specific alert given an id
     *
     * @param id - alert ID
     * @param jsonString - field-value mapping to pass as part of the request
     * */
    public void updateAlertByID(String id, String jsonString) throws Exception{

        UpdateRequest request = new UpdateRequest("alert-*", id);
        request.doc(jsonString, XContentType.JSON);

        UpdateResponse updateResponse = esClient.update(
                request, RequestOptions.DEFAULT);
    }

    /**
     * Create an alert object from a SearchHit
     *
     * @param hit - raw data
     * */
    private Alert createAlertDataObject(SearchHit hit){

        Map<String, Object> dataMap = hit.getSourceAsMap();

        Alert alert = new Alert();
        alert.setId(hit.getId());
        alert.setClassification((String)dataMap.get("service.name"));
        alert.setAlertType((String)dataMap.get("threat.alert_type"));
        alert.setLogType((String)dataMap.get("log.type"));
        alert.setEventCount(dataMap.get("threat.events.count").toString());
        alert.setDevice((String)dataMap.get("client.ip"));
        alert.setSite((String)dataMap.get("server.site"));
        alert.setUtcCreated((String)dataMap.get("event.timestamp_detected"));
        alert.setUtcEventStart((String)dataMap.get("event.start"));
        Map<String,String> clientGeo = (Map<String,String>)dataMap.get("client.geo");
        if(clientGeo != null) {
            alert.setCountryName(clientGeo.get("country_name"));
        }

        String severity = (String)dataMap.get("threat.severity");
        alert.setSeverity(severity);
        SeverityMap severityMap = SeverityMap.getBySeverity(severity);
        if(severityMap != null) {
            alert.setImpact(severityMap.getImpact());
            alert.setPriority(severityMap.getPriority());
        }

        return alert;
    }

}
