package au.com.telstra.sas.api.security;

import java.security.Principal;
import java.util.Objects;

import javax.ws.rs.core.SecurityContext;

/**
 * Basic implementation of JAX-RS SecurityContext interface, inserted into 
 * the incoming request by AuthenticationFilter.
 * 
 * @author d236359
 */
public class SimpleSecurityContext implements SecurityContext
{
	/**
	 * Initialize new instance.
	 */
	public SimpleSecurityContext(boolean secure, User user)
	{
		this.secure = secure;
		this.user = Objects.requireNonNull(user);
	}
	
	/**
	 * Get the authenticated user.
	 */
	@Override public Principal getUserPrincipal()
	{
		return user;
	}

	/**
	 * Is the user in the given role?
	 */
	@Override public boolean isUserInRole(String role)
	{
		// NB: Not using this method to determine user authorization.
		return false;
	}
	
	/**
	 * Was the request made on a secure channel?
	 */
	@Override public boolean isSecure()
	{
		return secure;
	}
	
	/**
	 * Authentication scheme name.
	 */
	@Override public String getAuthenticationScheme()
	{
		return "JWT Bearer";
	}

	// Authenticated user information supplied by AuthenticationFilter.
	private final User user;

	// Is the request secure (HTTPS)?
	private final boolean secure;
}
