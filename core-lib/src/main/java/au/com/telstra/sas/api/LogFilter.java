package au.com.telstra.sas.api;

import lombok.extern.log4j.Log4j2;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;

/**
 * JAX-RS request/response logging. Includes logging of the time it takes to process the request,
 * hence the @PreMatching and @Priority(0) annotations (attempting to capture as much of the process
 * time as possible).
 */
@PreMatching
@Provider
@Priority(0)
@Log4j2
public class LogFilter implements ContainerRequestFilter, ContainerResponseFilter
{
	/**
	 * Request filter.
	 */
	@Override public void filter(ContainerRequestContext requestContext) throws IOException
	{
		try
		{
			// Record current time in context properties.
			requestContext.setProperty(START_TIME_PROPERTY, System.currentTimeMillis());
		}
		catch (Exception ex)
		{
			// Under no circumstances should LogFilter break normal request processing!
			log.error("Request filter failed!", ex);
		}
	}

	/**
	 * Response filter.
	 */
	@Override public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException
	{
		try
		{
			// Compute total duration for request processing.
			Long startTime = (Long) requestContext.getProperty(START_TIME_PROPERTY);
			String durationString = (startTime == null) ? "?" : Long.toString(System.currentTimeMillis() - startTime);

			// Grab authenticated user.
			SecurityContext securityContext = requestContext.getSecurityContext();
			Principal user = (securityContext == null) ? null : securityContext.getUserPrincipal();
			String userString = (user == null) ? "(no user)" : user.toString();

			// Extract path and query portions of request URI (scheme/host/port are just wasted space in the log).
			URI requestUri = requestContext.getUriInfo().getRequestUri();
			String shortUri = requestUri.getRawPath(), queryParameters = requestUri.getRawQuery();
			if (queryParameters != null)
				shortUri += "?" + queryParameters;

			// And finally - write request information to the log.
			log.info("{} {} --> {} // {} ms, {}",
				requestContext.getMethod(),
				shortUri,
				responseContext.getStatus(),
				durationString,
				userString);
		}
		catch (Exception ex)
		{
			// Under no circumstances should LogFilter break normal request processing!
			log.error("Response filter failed!", ex);
		}
	}

	// ContainerRequestContext custom property names.
	private static final String START_TIME_PROPERTY = LogFilter.class.getName() + ".startTime";
}
