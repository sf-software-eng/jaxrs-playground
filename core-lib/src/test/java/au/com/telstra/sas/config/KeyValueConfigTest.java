package au.com.telstra.sas.config;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for KeyValueConfig.
 * 
 * @author d236359
 */
public class KeyValueConfigTest
{
	/**
	 * Make sure getStringArray(...) behaves correctly.
	 */
	@Test
	public void getStringArray()
	{
		List<String> list = Arrays.asList("abc", "def", "ghi");
		KeyValueConfig config = new KeyValueConfig("key", String.join(",", list));
		Assertions.assertTrue(() -> Arrays.asList(config.getStringArray("key")).equals(list));
	}
}
