package au.com.telstra.sas.api.security;

import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import au.com.telstra.sas.config.KeyValueConfig;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SigningKeyResolver;
import io.jsonwebtoken.SigningKeyResolverAdapter;
import lombok.extern.log4j.Log4j2;

/**
 * AuthorizationProvider implementation that validates and extracts claims from an encoded JWT string.
 * Configuration data is provided by the KeyValueConfig class. Expected configuration keys are:
 * 
 * // Comma-separated list of values from SignatureAlgorithm (case sensitive).
 * JWT_ALGORITHMS=HS256,RS256
 * 
 * // Key values for any algorithms listed by JWT_ALGORITHMS. All values are expected to be base64 encoded  
 * // strings. For HSxxx algorithms the decoded bytes must be suitable for use with SecretKeySpec. For RSxxx
 * // and ESxxx algorithms the bytes must be a valid X.509 SubjectPublicKeyInfo (X509EncodedKeySpec). 
 * JWT_HS256_KEY=...
 * JWT_RS256_KEY=...
 * 
 * @author d236359
 */
@ApplicationScoped
@Log4j2
public class JwtAuthorizationProvider implements AuthorizationProvider
{
	/**
	 * Default constructor (required for CDI).
	 */
	JwtAuthorizationProvider() throws Exception
	{
		this(new KeyValueConfig());
	}
	
	/**
	 * Initialize new provider instance with the provider configuration data.
	 */
	@Inject 
	public JwtAuthorizationProvider(KeyValueConfig config) throws Exception
	{
		// Load keys for enabled algorithms from configuration.
		Map<String, Key> keyMap = new HashMap<>();
		String[] enabledAlgorithmNames = config.getStringArray("JWT_ALGORITHMS");
		if (enabledAlgorithmNames != null)
		{
			for (String name : enabledAlgorithmNames)
			{
				byte[] keyBytes = Base64.getDecoder().decode(config.getString("JWT_" + name + "_KEY"));
				
				SignatureAlgorithm algorithm = SignatureAlgorithm.valueOf(name);
				if (algorithm.isHmac())
					keyMap.put(name, new SecretKeySpec(keyBytes, algorithm.getJcaName()));
				else if (algorithm.isRsa())
					keyMap.put(name, KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(keyBytes)));
				else if (algorithm.isEllipticCurve())
					keyMap.put(name, KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(keyBytes)));
				else
					throw new RuntimeException("Don't know how to decode key bytes for JWT algorithm " + name + " !");
			}
		}
		log.debug("init: keyMap={}", keyMap);
		
		// JJWT key lookup interface.
		SigningKeyResolver resolver = new SigningKeyResolverAdapter()
		{
			@Override public Key resolveSigningKey(JwsHeader header, Claims claims) 
			{
				log.debug("resolve: header={} claims={}", header, claims);
				return keyMap.get(header.getAlgorithm());
			}
		};
		
		// Create JWT parser.
		this.jwtParser = Jwts.parserBuilder()
			.setSigningKeyResolver(resolver)
			.build();
	}

	/**
	 * Validate the given Authorization header credentials, and return a User object for the identified
	 * user. If the provided credentials are invalid then this method must throw AuthorizationException,
	 * which will trigger a 401 response back to the caller. All other exceptions will result in a 500
	 * response.
	 */
	@Override public User validateCredentials(String credentials) throws Exception
	{
		log.debug("validateCredentials: credentials={}", credentials);
		
		// Validate credentials auth scheme, and extract encoded JWT string.
		Matcher m = AUTH_HEADER_PATTERN.matcher(credentials);
		if (!m.matches())
			throw new AuthorizationException("Error extracting JWT string from credentials!", null);
		String tokenString = m.group(1);
		log.debug("validateCredentials: tokenString={}", tokenString);

		try
		{
			// Parse encoded JWT string.
			Claims claims = jwtParser.parseClaimsJws(tokenString).getBody();
			log.debug("validateCredentials: claims={}", claims);
			
			// Extract, wrap and return claims.
			return new User(
				claims.get(USER_ID_CLAIM, String.class),
				Role.fromApiName(claims.get(USER_ROLE_CLAIM, String.class)),
				claims.get(TENANT_ID_CLAIM, String.class),
				claims.get(TENANT_NAME_CLAIM, String.class));
		}
		catch (JwtException | IllegalArgumentException ex)
		{
			// NB: Annoyingly JJWT throws IllegalArgumentException if there's no key available for the
			//     algorithm in the JWT header, instead of a JwtException subclass :-P
			throw new AuthorizationException("Error parsing encoded JWT string!", ex);
		}
	}
	
	/**
	 * Get challenge string to send back in 401 response WWW-Authenticate header.
	 */
	@Override public String getChallenge()
	{
		return "Bearer realm=\"OpenMSS\"";
	}

	// Configured JJWT parser instance (JJWT parser is thread-safe so we only need to create one instance).
	private final JwtParser jwtParser;

	// Auth header regex.
	// NB: Doesn't aim to fully validate the token content (that's JJWT's job!) - just want
	//     to ensure that the auth header starts with "Bearer" and extract the token content.
	static final Pattern AUTH_HEADER_PATTERN = Pattern.compile("(?i)Bearer\\s+(\\S+)");

	// JWT claim keys.
	static final String USER_ID_CLAIM = "user_id";
	static final String USER_ROLE_CLAIM = "user_role";
	static final String TENANT_ID_CLAIM = "tenant_id";
	static final String TENANT_NAME_CLAIM = "tenant_name";
}
