package au.com.telstra.sas.api;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for RestExceptionMapper.
 * 
 * @author d236359
 */
public class RestExceptionMapperTest
{
	/**
	 * Check that the given exception produces the expected HTTP response code.
	 */
	private static void test(Response.Status status, Exception ex)
	{
		Response response = (new RestExceptionMapper()).toResponse(ex);
		Assertions.assertEquals(status.getStatusCode(), response.getStatus());
	}
	
	/**
	 * Test with NotAuthorizedException.
	 */
	@Test
	public void notAuthorizedException()
	{
		test(Response.Status.UNAUTHORIZED, new NotAuthorizedException("Bearer foo"));
	}
	
	/**
	 * Test with BadRequestException.
	 */
	@Test
	public void badRequestException()
	{
		test(Response.Status.BAD_REQUEST, new BadRequestException());
	}
	
	/**
	 * Test with NumberFormatException.
	 */
	@Test
	public void numberFormatException()
	{
		test(Response.Status.INTERNAL_SERVER_ERROR, new NumberFormatException());
	}
}
