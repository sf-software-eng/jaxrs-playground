package au.com.telstra.sas.api.security;

/**
 * Interface for a service that provides validation of credentials provided by HTTP Authorization header
 * to the AuthenticationFilter, and generation of a suitable User object for the same.
 * 
 * @author d236359
 */
public interface AuthorizationProvider
{
	/**
	 * Validate the given Authorization header credentials, and return a User object for the identified
	 * user. If the provided credentials are invalid then this method must throw AuthorizationException,
	 * which will trigger a 401 response back to the caller. All other exceptions will result in a 500
	 * response.
	 */
	public User validateCredentials(String credentials) throws Exception;
	
	/**
	 * Get challenge string to send back in 401 response WWW-Authenticate header.
	 */
	public String getChallenge();
}
